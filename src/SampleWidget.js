import React from 'react';
import ReactDOM from 'react-dom';
import App from './App'

export default function createWidget(widgetParams) {
    
    const WelcomeSnippet = () => {
        return <App widgetParams={widgetParams} />
    }
   
    ReactDOM.render(<WelcomeSnippet />, document.getElementById('sample-widget-app'))


}
