import React, {useState} from 'react';

const gridStyle = {
  display: "grid",
  gridTemplateColumns: 'repeat(2, 1fr)',
  gap: '50%',
}

const buttonStyle = {
  backgroundColor: 'rgb(57 124 183 / 79%)',
  border: 'none',
  outline: 'none',
  padding: '10px 0px',
  fontFamily: 'Arial, Helvetica, sans-serif'
}

const Stepper = ({steps}) => {
    const [currentStep, setCurrentStep] = useState(1);
    const goNextStep = () => {
        const nextStep = currentStep + 1;
        if (nextStep <= steps.length) {
          setCurrentStep(nextStep);
        }
      };
     
      const goPreviousStep = () => {
        const previousStep = currentStep - 1;
        if (previousStep >= 1) {
          setCurrentStep(previousStep);
        }
      };

      return (
        <div>
          {steps.map((step, i) => (
            <div key={i}>
              {currentStep === i+1 && <step.element
                step={i + 1}
                currentStep={currentStep}
                setCurrentStep={setCurrentStep}
              />}
            </div>
          ))}
        <div style={gridStyle}>
            <button onClick={goPreviousStep} style={buttonStyle} disabled={currentStep === 1} >Back</button>
            <button onClick={goNextStep} style={buttonStyle} disabled={currentStep === steps.length}>Next</button>
        </div>
        </div>
      )
}

export default Stepper;