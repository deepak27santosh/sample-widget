import React from 'react';

const buttonStyle = {
    backgroundColor: 'transparent',
    outline: 'none',
    border: 'none',
    color: 'blue'
}

const textStyle = {
    fontFamily: 'sans-serif',
    fontSize: 'large'
}

const FinalStep = ({step, setCurrentStep, previousStep}) => {
    const handleOnClick = () => {
        setCurrentStep(previousStep);
    }
    return(
        <div>
            <p style={textStyle}> Hey this is Step {step}
            <button onClick={handleOnClick} style={buttonStyle}>Jump to Step 1</button>
            </p>
        </div>
)}
export default FinalStep; 