import React from 'react';

const textStyle = {
    fontFamily: 'sans-serif',
    fontSize: 'large'
}
const SecondStep = ({step}) => (
    <div>
        <p style={textStyle}>
            Hey this is Step {step}
        </p>
    </div>
)
export default SecondStep; 