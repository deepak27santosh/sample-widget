import React, { useState } from 'react';
import Stepper from './Stepper';
import InitialStep from './InitialStep';
import FinalStep from './FinalStep';
import SecondStep from './SecondStep';

const buttonContainerStyle = {
  display: 'block',
  marginLeft: 'auto',
  marginRight: 0,
}

const buttonStyle = {
  backgroundColor: 'transparent',
  outline: 'auto'
  }

const modalStyle = {
  position: 'fixed',
  zIndex: 1,
  left: 0,
  top: 0,
  width: '100%',
  height: '100%',
  overflow: 'auto',
  backgroundColor: 'rgb(0,0,0)',
  backgroundColor: 'rgba(0,0,0,0.4)',
}

const modalContentStyle = {
  backgroundColor: '#fefefe',
  margin: '15% auto',
  padding: '20px',
  border: '1px solid #888',
  width: '30%',
}

const gridStyle = {
  display: "grid",
  gridTemplateColumns: 'repeat(2, 1fr)',
}

const textStyle={
  fontFamily: 'sans-serif',
  fontSize: 'large'
}

const App = ({widgetParams}) => {
    const [isOpen, setIsOpen] = useState(true);
    const handleClick = () => {
      setIsOpen(false);
    }
    const steps = [
      {
        title: "Initial Step",
        element: stepProps => <InitialStep {...stepProps}/>,
      },
      {
        title: "Second Step",
        element: stepProps => <SecondStep {...stepProps}/>,
      },
      {
        title: "Final Step",
        element: stepProps => <FinalStep {...stepProps} previousStep={1}/>,
      },
    ];
    return(
      isOpen && 
            <div style={modalStyle}>
              <div style={modalContentStyle}>
                <div style={gridStyle}>
                  <div style={textStyle}>
                    Hello {widgetParams.appName}
                  </div>
                  <div style={buttonContainerStyle}>
                    <button onClick={handleClick} style={buttonStyle}>X</button>
                  </div>
                </div>
                <div>
                  <Stepper steps={steps}/>
                </div>
              </div>
            </div>
          );
}

export default App;